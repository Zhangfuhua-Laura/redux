import React from 'react';
import ProductDetails from "./product/pages/ProductDetails";
import {BrowserRouter as Router} from "react-router-dom";
import {Route, Switch} from "react-router";
import Home from "./home/Home";
import UserProfiles from "./userProfiles/pages/UserProfiles";

const App = () => {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/product-details" component={ProductDetails} />
          <Route path="/" exact component={Home}/>
          <Route path="/user-profiles/:id" component={UserProfiles}/>
        </Switch>
      </Router>
    </div>
  );
};

export default App;