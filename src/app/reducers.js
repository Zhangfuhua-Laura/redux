import { combineReducers } from 'redux';
import productReducer from './product/reducers/productReducer';
import userReducer from './userProfiles/reducers/userReducers';
export default combineReducers({
  product: productReducer,
  user: userReducer
});