export const getUserProfiles = (id) => (dispatch) => {
    fetch(`http://localhost:8080/api/user-profiles/${id}`)
        .then(response => response.json())
        .then(result => {
            dispatch({
                type: 'GET_USER_PROFILE',
                userProfiles: result
            });
        });
}