import React from 'react';

const UserProfilesDetails = ({username, gender, description}) => {

    return (<div>
        <div>
            <label>User Name: </label>
            <span>{username}</span>
        </div>
        <div>
            <label>Gender: </label>
            <span>{gender}</span>
        </div>
        <div>
            <label>Description: </label>
            <span>{description}</span>
        </div>
    </div>)
};

export default UserProfilesDetails;