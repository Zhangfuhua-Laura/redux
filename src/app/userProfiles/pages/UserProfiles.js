import React, {Component} from 'react';
import UserProfilesDetails from "../components/UserProfilesDetails";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserProfiles} from "../action/userActions";

class UserProfiles extends Component {

    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.getUserProfiles(id);
    }

    constructor(props, context) {
        super(props, context);
    }

    render() {
        const {name, gender, description} = this.props.userProfiles;
        return (
            <div>
                <h3>User Profile</h3>
                <UserProfilesDetails username={name} gender={gender} description={description}/>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    userProfiles: state.user.userProfiles
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getUserProfiles
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfiles);
